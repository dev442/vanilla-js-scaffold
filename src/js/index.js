import config from 'visual-config-exposer';

const greeting = config.settings.greeting;

const title = `
    <h1>${greeting}</h1>
`;
const image = `
    <img src="${config.settings.greetingImage}" alt="image"/>
`;

const root = document.getElementById('app');

root.innerHTML = title + image;
